package part1;
public class Aims {
	
	    public static void main(String[] args){
	        Order anOrder = new Order("1/12/2010");
	        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
	        dvd1.setCategory("Animation");
	        dvd1.setCost(19.95f);
	        dvd1.setDirector("Roger Allers");
	        dvd1.setLength(87);
	        anOrder.addDigitalVideoDisc(dvd1);

	        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
	        dvd2.setCategory("Science Fiction");
	        dvd2.setCost(24.95f);
	        dvd2.setDirector("Geogre Lucas");
	        dvd2.setLength(124);
	        anOrder.addDigitalVideoDisc(dvd2);

	        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
	        dvd3.setCategory("Animation");
	        dvd3.setCost(18.99f);
	        dvd3.setDirector("John Musker");
	        dvd3.setLength(90);

	        anOrder.addDigitalVideoDisc(dvd3);

	        System.out.print("Total Cost is: ");
	        System.out.println(anOrder.totalCost());
	        anOrder.removeDigitalVideoDisc(dvd1);
	        System.out.print("Total Cost is: ");
	        System.out.println(anOrder.totalCost());
	        anOrder.print(); 
	        Order Order1=new Order("3/4/2008");
	        Order1.addDigitalVideoDisc(anOrder.getItemsOrdered());
	        Order Order2=new Order("20/4/2020");
	        Order2.addDigitalVideoDisc(dvd1,dvd2);
	        Order2.print();
	        Order anOrder4=new Order("11/11/2011");
	        
	    }
	}

