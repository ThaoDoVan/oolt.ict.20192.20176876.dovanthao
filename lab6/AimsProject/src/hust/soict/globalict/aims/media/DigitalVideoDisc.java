package hust.soict.globalict.aims.media;



public class DigitalVideoDisc extends Media {
private int length;
public String director;

public String getDirector() {
	return director;
}
public void setDirector(String director) {
	this.director = director;
}
public int getLength() {
	return length;
}
public void setLength(int length) {
	this.length = length;
}

public DigitalVideoDisc(String title) {
	super(title);
}
public DigitalVideoDisc(String title,String category) {
	super(title,category);
}
public DigitalVideoDisc(String title,String category,String director) {
	super(title,category);
	this.director=director;
	
}
public DigitalVideoDisc(int id ,String title,String category,float cost,String director,int length) {
	super(id,title,category,cost);
	this.director=director;
	this.length=length;
}	

}


