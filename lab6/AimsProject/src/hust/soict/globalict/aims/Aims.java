package hust.soict.globalict.aims;
import hust.soict.globalict.aims.media.*;
import hust.soict.globalict.aims.order.Order;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.List;

public class Aims {
	public static void showMenu() {

		System.out.println("Order Management Application: ");

		System.out.println("--------------------------------");

		System.out.println("1. Create new order");

		System.out.println("2. Add item to the order");

		System.out.println("3. Delete item by id");

		System.out.println("4. Display the items list of order");

		System.out.println("0. Exit");

		System.out.println("--------------------------------");

		System.out.println("Please choose a number: 0-1-2-3-4");

		}
	public static void main(String[] args) {	
		Scanner scanner = new Scanner(System.in);
		Order order=new Order();
		int choice=0;
		do {
			showMenu();
			choice=scanner.nextInt();
		
		switch(choice)
		{
		case 1:
			
			System.out.println("ID:\n");
			int orderid =scanner.nextInt();
			order.setId(orderid);
			
			System.out.printf("%d order is created\n",order.getId());
			break;
		case 2:
			String bookauthor,mediatitle,mediacategory;
			int mediaid;
			float mediacost;
			System.out.println("Choose the item which you want to add:");
			System.out.println("1.Book  2.Disc ");
		    int n=1;
		    while(true) {
		    	
			
		    n=scanner.nextInt();
		    scanner.hasNextLine();
		  
		    
		    
		   if(n==1) {
			   System.out.println("ID:");
			    mediaid=scanner.nextInt();
			    scanner.nextLine();
			    System.out.println("TITLE:");
			    mediatitle=scanner.nextLine();
			    System.out.println("CATEGORY:");
			    mediacategory=scanner.nextLine();
			    System.out.println("COST:");
			    mediacost=scanner.nextFloat();
			    scanner.nextLine();
			System.out.println("AUTHOR:");  
		    bookauthor = scanner.nextLine();
		    List<String> bookauthors= new ArrayList<String>();
		    bookauthors.add(bookauthor);
		    Book book=new Book(mediaid,mediatitle,mediacategory,mediacost,bookauthors);	
		    order.addMedia(book);
		    }
		   if(n==2)
		   {System.out.println("ID:");
		    mediaid=scanner.nextInt();
		    scanner.nextLine();
		    System.out.println("TITLE:");
		    mediatitle=scanner.nextLine();
		    System.out.println("CATEGORY:");
		    mediacategory=scanner.nextLine();
		    System.out.println("COST:");
		    mediacost=scanner.nextFloat();
		    scanner.nextLine();
		    System.out.println("LENGTH:");
		    int disclength=scanner.nextInt();
		    scanner.nextLine();
		    System.out.println("DIRECTOR:");
		    String discdirector=scanner.nextLine() ;        
		    DigitalVideoDisc disc= new DigitalVideoDisc(mediaid,mediatitle,mediacategory,mediacost,discdirector,disclength); 
		      order.addMedia(disc);
		   }
		   else
			  break;
		   
		    
		    }
		   
		 break;
		case 3:
			
			int temp;
			boolean isdel =false;
			System.out.println("Enter id which you want to delete");
			temp=scanner.nextInt();
			for(int i=0;i<order.getItemsOrdered().size();i++)
			{if(order.getItemsOrdered().get(i).getId()==temp)
			{  isdel=true;
				order.removeMedia(order.getItemsOrdered().get(i));;
			}}
			if(isdel)
			{System.out.println("The item has already been deleted");}
			else
				System.out.println("Not found item");
			break;
			
		case 4:
			for (Media media : order.getItemsOrdered()) {
                System.out.println("Id: " +media.getId());
                System.out.println("Title: " + media.getTitle());
                System.out.println("Category: " + media.getCategory());
                System.out.println("Cost: " + media.getCost());
                
           
               if (media instanceof Book)
                 for(String author : ((Book) media).getAuthors()) 
                 { System.out.println("Authors:" +author);}
 
                if( media instanceof DigitalVideoDisc)
			     {System.out.println("Length:"+((DigitalVideoDisc) media).getLength());}
                if( media instanceof DigitalVideoDisc)
			     {System.out.println("Director:"+((DigitalVideoDisc) media).getDirector());}   
			}      
            break ;
		case 0:
			break;
			
		 default:
			 System.out.println("Try again with choice from 0 to 4");
			 break;
		}
		
	}
	while(choice!=0);
	
	
	}
}

