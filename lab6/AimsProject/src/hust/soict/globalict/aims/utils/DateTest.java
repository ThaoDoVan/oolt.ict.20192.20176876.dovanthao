package hust.soict.globalict.aims.utils;

public class DateTest {
	public static void main(String[] args) {
		MyDate mydate = new MyDate();

		System.out.printf("MyDate with no parameter: %d/%d/%d\n", mydate.getDay(), mydate.getMonth(), mydate.getYear());

		mydate = new MyDate(1, 1, 2020);
		System.out.printf("MyDate with 3 parameters : %d/%d/%d\n", mydate.getDay(), mydate.getMonth(),
				mydate.getYear());

		mydate = new MyDate("February 18th 2019");
		System.out.printf("Mydate with a string parameter : %d/%d/%d\n", mydate.getDay(), mydate.getMonth(),
				mydate.getYear());
		mydate.setMonth("June");
		System.out.printf("%d\n", mydate.getMonth());
		System.out.println("Current date is : ");
		mydate.print();
		System.out.println("String version is: ");
		mydate.printString();
		MyDate mydate1 = new MyDate(19, 05, 1999);
		MyDate mydate2 = new MyDate("January 1st 2000");
		MyDate mydate3 = new MyDate(2, 2, 2002);
		MyDate[] Datelist = new MyDate[] { mydate1, mydate2, mydate3 };
		DateUtils.sortDates(Datelist);
		System.out.println("The array date is sorted :");
		for (int i = 0; i < Datelist.length; i++) {
			System.out.printf("%d/%d/%d\n", Datelist[i].getDay(), Datelist[i].getMonth(), Datelist[i].getYear());
		}
	}
}
