package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Book extends Media implements Comparable {
    private List<String> authors = new ArrayList<String>();
	private String content;
	private List<String> contentTokens = new ArrayList<String>();
	private Map<String, Integer> wordFrequency = new TreeMap<String, Integer>();
	public Book(String title,String category,float cost,List<String> authors) {
	super(title,category,cost);
	this.authors=authors;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Book(String title, String category, float cost) {
		super(title, category, cost);
	}
	public Book(int id, String title, String category,float cost,List<String> authors) {
        super(id, title, category,cost);
        this.authors = authors;
    }
	public List<String> getAuthors() {
		return authors;
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	public void addAuthor(String authorName) {
		int i=0;
		boolean flat = true ;
		while(i<authors.size()){        
		if(authorName.equals(authors.get(i))==true)
			flat=true;
		else 
			this.getAuthors().add(authorName);	
		i++;}
		if(flat)
		System.out.printf("%s is already existed\n",authorName);
		 
}
	public void removeAuthor(String authorName) {
		int i=0;
		boolean flat=true;
		while(i<authors.size())
		{if(authorName.equals(authors.get(i))== true)
			{flat=true;
			this.getAuthors().remove(authorName);}
		i++;}
	   if(flat) 
		   System.out.printf("%s is removed\n",authorName);
	   else
		   System.out.printf("%s is not in the list\n",authorName);
	}

    public Book(String title, String content) {
        super(title);
        this.content = content;
    }
	 public int compareTo(Object o) {
	        Book book = (Book) o;
	        return this.getTitle().compareTo(book.getTitle());
	    }
	 public void processContent() {
	        contentTokens = new ArrayList<String>();
	        String[] tokens = content.split(" ");
	        for (String key : tokens) {
	            contentTokens.add(key);
	        
	    	if(!wordFrequency.containsKey(key)) 
	 
	    		wordFrequency.put(key, 1);
			else wordFrequency.put(key, wordFrequency.get(key)+1);
		}
	    	Collections.sort(contentTokens);
	        }
	        

	    	public String toString() {
	    		return "Book: title= " + getTitle() + ", cost = " + getCost() + ", contentLength = " + contentTokens.size()
	    				+ "\nToken list: " + contentTokens + 
	    				"\nword Frequency: " + wordFrequency;
	    	}
	}
	
	
	
	
	
	
	


