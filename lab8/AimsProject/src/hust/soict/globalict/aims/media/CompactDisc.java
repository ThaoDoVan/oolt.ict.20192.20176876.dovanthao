package hust.soict.globalict.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable,Comparable{
private String artist;
public String getArtist() {
	return artist;
}

public void setArtist(String artist) {
	this.artist = artist;
}

private int length;
private ArrayList<Track> tracks = new ArrayList<Track>();

public CompactDisc(int id, String title,String category,float cost,int length,String director,String artist) {
  super(id,title,category,cost,length,director);
  this.artist=artist;
		
	}
public CompactDisc(String title,String artist) {
	super(title);
	this.artist=artist;
	
}
public void addTrack(Track track) {
if(!this.tracks.contains(track)) 
	this.tracks.add(track);
else 
 System.out.println("Track is already existed");
	}	
public void removeTrack(Track track)
{if(tracks.contains(track))
	this.tracks.remove(track);
else
	System.out.println("Not found track");
}

public int getlength()
{ length=0;
for (Track track : this.tracks) {
    length += track.getLength();
}
return length;

 }
@Override
public void play()
{	for(int i=0; i<tracks.size(); i++) 
{System.out.println("Track:"+tracks.get(i).getTitle()+" Length:"+tracks.get(i).getTitle());
}
}
public int compareTo(Object o) {
    CompactDisc compactDisc = (CompactDisc) o;
    return (this.tracks.size()- compactDisc.tracks.size());
}
}