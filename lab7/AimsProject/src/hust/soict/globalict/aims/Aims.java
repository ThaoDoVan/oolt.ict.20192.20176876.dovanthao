package hust.soict.globalict.aims;
import hust.soict.globalict.aims.media.*;
import hust.soict.globalict.aims.order.Order;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.List;

public class Aims {
	public static void showMenu() {

		System.out.println("Order Management Application: ");

		System.out.println("--------------------------------");

		System.out.println("1. Create new order");

		System.out.println("2. Add item to the order");

		System.out.println("3. Delete item by id");

		System.out.println("4. Display the items list of order");

		System.out.println("0. Exit");

		System.out.println("--------------------------------");

		System.out.println("Please choose a number: 0-1-2-3-4");

		}
		public static void showminiMenu() {
			System.out.println("1.Book");
			System.out.println("2.DVD");
			System.out.println("3.CD");
			System.out.println("Choose type of media");
		}
	public static void main(String[] args) {
		Thread thread=new Thread( new MemoryDaemon());
        thread.setDaemon(true);
        thread.start();
		Scanner scanner = new Scanner(System.in);
		Order order=new Order();
		int choice=0;
		do {
			showMenu();
			choice=scanner.nextInt();
		
		switch(choice)
		{
		case 1:
			 order=new Order();
			System.out.println("ID:\n");
			int orderid =scanner.nextInt();
			order.setId(orderid);
			
			System.out.printf("%d order is created\n",order.getId());
			break;
		case 2:
			String bookauthor,mediatitle,mediacategory,mediaartist;
			int mediaid;
			float mediacost;
		    showminiMenu();
		  
		    int n=scanner.nextInt();
		    scanner.hasNextLine();
		    System.out.println("ID:");
		    mediaid=scanner.nextInt();
		    scanner.nextLine();
		    System.out.println("TITLE:");
		    mediatitle=scanner.nextLine();
		    System.out.println("CATEGORY:");
		    mediacategory=scanner.nextLine();
		    System.out.println("COST:");
		    mediacost=scanner.nextFloat();
		    scanner.nextLine();
		    
		    switch(n) {
		case 1: 
			 
			System.out.println("AUTHOR:");  
		    bookauthor = scanner.nextLine();
		    List<String> bookauthors= new ArrayList<String>();
		    bookauthors.add(bookauthor);
		    Book book=new Book(mediaid,mediatitle,mediacategory,mediacost,bookauthors);	
		    order.addMedia(book);
		    break;
		    
		case 2:
		   
		    System.out.println("LENGTH:");
		    int disclength=scanner.nextInt();
		    scanner.nextLine();
		    System.out.println("DIRECTOR:");
		    String discdirector=scanner.nextLine() ;        
		    DigitalVideoDisc disc= new DigitalVideoDisc(mediaid,mediatitle,mediacategory,mediacost,discdirector,disclength); 
		      order.addMedia(disc);
		      System.out.println("Do you want to play DVD ? Y/N: ");
		  	char c = scanner.next().charAt(0);
			switch(c) {
				case 'Y': 
					disc.play();
					break;
				
				default: break;
			}
			break;
			
		   
		case 3:
		   { 
           System.out.println("Artist: ");
           mediaartist = scanner.nextLine();
           CompactDisc compactDisc = new CompactDisc( mediatitle, mediaartist);
           System.out.println("Input number of tracks:");
           int num=scanner.nextInt();
           scanner.nextLine();
           for(int i=0;i<num;i++) {
        		System.out.println("Input title of track : ");
				String trackTitle = scanner.nextLine();
				System.out.println("Input length of track: ");
				int trackLength = scanner.nextInt();
				scanner.nextLine();
				Track track=new Track(trackTitle,trackLength);
           }
           order.addMedia(compactDisc);
       	System.out.println("Do you want to play CD ? Y/N: ");
	  	char c1 = scanner.next().charAt(0);
			switch(c1) {
				case 'Y': 
					compactDisc.play();
					break;
				
				default: break;
			}
           
			   
		   } 
		   
		   
		default: break;
		    
		    
		   
		    }
		    break;
		case 3:
			
			int temp;
			boolean isdel =false;
			System.out.println("Enter id which you want to delete");
			temp=scanner.nextInt();
			for(int i=0;i<order.getItemsOrdered().size();i++)
			{if(order.getItemsOrdered().get(i).getId()==temp)
			{  isdel=true;
				order.removeMedia(order.getItemsOrdered().get(i));;
			}}
			if(isdel)
			{System.out.println("The item has already been deleted");}
			else
				System.out.println("Not found item");
			break;
			
		case 4:
			for (Media media : order.getItemsOrdered()) {
                System.out.println("Id: " +media.getId());
                System.out.println("Title: " + media.getTitle());
                System.out.println("Category: " + media.getCategory());
                System.out.println("Cost: " + media.getCost());
                
           
               if (media instanceof Book)
                 for(String author : ((Book) media).getAuthors()) 
                 { System.out.println("Authors:" +author);}
 
                if( media instanceof DigitalVideoDisc)
			     {System.out.println("Length:"+((DigitalVideoDisc) media).getLength());}
                if( media instanceof DigitalVideoDisc)
			     {System.out.println("Director:"+((DigitalVideoDisc) media).getDirector());}  
                if (media instanceof CompactDisc) {
                    System.out.println("Artist: " + ((CompactDisc) media).getArtist());
                    System.out.print("Length of  tracks : ");
                    System.out.println(((CompactDisc) media).getLength() + " m");
			}      
            break ;}
		case 0:
			break;
			
		 default:
			 System.out.println("Try again with choice from 0 to 4");
			 break;
		}
		
	}
	while(choice!=0);
	
	
	
}
		}

