package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media {
    private List<String> authors = new ArrayList<String>();
	public Book(String title,String category,float cost,List<String> authors) {
	super(title,category,cost);
	this.authors=authors;
	}
	public Book() {
	}
	public Book(int id, String title, String category,float cost,List<String> authors) {
        super(id, title, category,cost);
        this.authors = authors;
    }
	public List<String> getAuthors() {
		return authors;
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	public void addAuthor(String authorName) {
		int i=0;
		boolean flat = true ;
		while(i<authors.size()){        
		if(authorName.equals(authors.get(i))==true)
			flat=true;
		else 
			this.getAuthors().add(authorName);	
		i++;}
		if(flat)
		System.out.printf("%s is already existed\n",authorName);
		 
}
	public void removeAuthor(String authorName) {
		int i=0;
		boolean flat=true;
		while(i<authors.size())
		{if(authorName.equals(authors.get(i))== true)
			{flat=true;
			this.getAuthors().remove(authorName);}
		i++;}
	   if(flat) 
		   System.out.printf("%s is removed\n",authorName);
	   else
		   System.out.printf("%s is not in the list\n",authorName);
	}
	
	
	
	
	}
	
	
	


