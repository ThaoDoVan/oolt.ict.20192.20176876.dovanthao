package part1;

public class Order {
	public static final int MAX_NUMBERS_ORDERED=10;
    private DigitalVideoDisc itemsOrdered[]=new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
    public int qtyOrdered;
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc){
		 if(qtyOrdered < MAX_NUMBERS_ORDERED){
	            itemsOrdered[qtyOrdered++] = disc;
	            System.out.println("The disc has been added");
	        }
	        else 
	        	System.out.println("The order is almost full");
		
	}
	  public void removeDigitalVideoDisc(DigitalVideoDisc disc){
	        int i = 0;
	        boolean flat = false;
	        while(i < qtyOrdered){
	            if(itemsOrdered[i] == disc){
	                flat = true;
	                for(int j=i; j<qtyOrdered-1; j++) 
	                itemsOrdered[j] = itemsOrdered[j+1]; 
	                qtyOrdered--;
	            }
	            i++;
	        }

	        if(flat) 
	        	System.out.println("Disc was removed");
	        else
	        	System.out.println("Not exist disc !");
	    } 
	        
	
	public float totalCost() {
		  float total = 0;
	      for(int i=0; i<qtyOrdered; i++) 
	    	  total += itemsOrdered[i].getCost();
	      return  total;
		
	}
}