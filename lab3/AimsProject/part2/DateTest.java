package part2 ;

public class DateTest {
    public static void main(String[] args) {
        MyDate mydate = new MyDate();
        System.out.printf("MyDate with no parameter: %d/%d/%d\n", mydate.getDay(), mydate.getMonth(), mydate.getYear());

        mydate = new MyDate(1, 1, 2020);
        System.out.printf("MyDate with 3 parameters : %d/%d/%d\n", mydate.getDay(), mydate.getMonth(), mydate.getYear());

        mydate=new MyDate("February 18th 2019");
        System.out.printf("Mydate with a string parameter : %d/%d/%d\n", mydate.getDay(), mydate.getMonth(), mydate.getYear());
    }
}
