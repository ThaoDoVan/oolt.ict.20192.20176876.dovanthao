package hust.soict.globalict.aims.order;

import hust.soict.globalict.aims.disc.DigitalVideoDisc;
import java.util.Random;

public class Order {
	public static final int MAX_NUMBERS_ORDERED=10;
	public static final int MAX_LIMITED_ORDERED = 5;
	private static int nbOrders = 0;
  
	private DigitalVideoDisc itemsOrdered[]=new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
    public int qtyOrdered;
    private String dateOrdered;
	

    public Order(String dateOrdered) {
        if (nbOrders < MAX_LIMITED_ORDERED) {
            this.dateOrdered = dateOrdered;
            nbOrders++;
            System.out.println("The order has been added");
        } else
            System.out.println("Reach the max ordered, can not add this order ");
    }




	public String getDateOrdered() {
		return dateOrdered;
	}



	public void setDateOrdered(String dateOrdered) {
		this.dateOrdered = dateOrdered;
	}



	public int getQtyOrdered() {
		return qtyOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc){
		 if(qtyOrdered < MAX_NUMBERS_ORDERED){
	            itemsOrdered[qtyOrdered++] = disc;
	            System.out.println("The disc has been added");
	        }
	        else 
	        	System.out.println("The order is almost full");
		
	}
	 public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
	        for (int i = 0; i < dvdList.length; i++) {
	            addDigitalVideoDisc(dvdList[i]);
	        }
	    }
	 public void addDigitalVideoDisc(DigitalVideoDisc disc1 ,DigitalVideoDisc disc2) {
		 addDigitalVideoDisc(disc1);
		 addDigitalVideoDisc(disc2);
	 }
	
	  public void removeDigitalVideoDisc(DigitalVideoDisc disc){
	        int i = 0;
	        boolean flat = false;
	        while(i < qtyOrdered){
	            if(itemsOrdered[i] == disc){
	                flat = true;
	                for(int j=i; j<qtyOrdered-1; j++) 
	                itemsOrdered[j] = itemsOrdered[j+1]; 
	                qtyOrdered--;
	            }
	            i++;
	        }

	        if(flat) 
	        	System.out.println("Disc was removed");
	        else
	        	System.out.println("Not exist disc !");
	    } 
	        
	
	public float totalCost() {
		  float total = 0;
	      for(int i=0; i<qtyOrdered; i++) 
	    	  total += itemsOrdered[i].getCost();
	      return  total;
		
	}
	 public void print() {
	        System.out.println("***********Order************");
	        System.out.println("Date: " + dateOrdered);
	        System.out.println("Order items: ");
	        for (int i = 0; i < qtyOrdered; i++) {
	            System.out.println(i + 1 + ". DVD - " +itemsOrdered[i].getTitle() + " - " + itemsOrdered[i].getCategory()
	                    + " - " + itemsOrdered[i].getDirector() + " - " + itemsOrdered[i].getLength() + " : "
	                    + itemsOrdered[i].getCost() + "$");
	        }
	        System.out.println("Total cost: " +totalCost());
	        System.out.println("****************************");
} 
	public DigitalVideoDisc getALuckyItem() {
	Random rand = new Random();
	int i = rand.nextInt(qtyOrdered);
	return itemsOrdered[i];    	
		}
}
	 