package hust.soict.globalict.aims;
import hust.soict.globalict.aims.disc.*;
import hust.soict.globalict.aims.order.*;
public class DiskTest{
		public static void main(String[] args) {
	Order anOrder = new  Order("1/12/2010");
    DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
    dvd1.setCategory("Animation");
    dvd1.setCost(19.95f);
    dvd1.setDirector("Roger Allers");
    dvd1.setLength(87);

    DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
    dvd2.setCategory("Science Fiction");
    dvd2.setCost(24.95f);
    dvd2.setDirector("Geogre Lucas");
    dvd2.setLength(124);

    DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
    dvd3.setCategory("Animation");
    dvd3.setCost(18.99f);
    dvd3.setDirector("John Musker");
    dvd3.setLength(90);
    anOrder.addDigitalVideoDisc(dvd3);
    anOrder.addDigitalVideoDisc(dvd1, dvd2);
    float cost=anOrder.totalCost();
    DigitalVideoDisc luckyDvd = anOrder.getALuckyItem();
    
   System.out.println("The random free item is "+luckyDvd.getTitle()+" "+ luckyDvd.getCategory()+" "+luckyDvd.getDirector()+" "+luckyDvd.getLength());
   System.out.println("Total cost is still "+cost);
	
}
}
