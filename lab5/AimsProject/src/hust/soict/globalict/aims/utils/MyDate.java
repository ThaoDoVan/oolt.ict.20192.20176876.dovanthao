package hust.soict.globalict.aims.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class MyDate {
	private int day;
	private int month;
	private int year;
	Date now = new Date();
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
	String time = simpleDateFormat.format(now);
	String[] temp = time.split("/");

	public MyDate() {
		this.day = Integer.parseInt(temp[0]);
		this.month = Integer.parseInt(temp[1]);
		this.year = Integer.parseInt(temp[2]);
	}

	public MyDate(int day, int month, int year) {
		if (day > 0 && day < 32 && month > 0 && month < 13 && year > 0) {
			this.day = day;
			this.month = month;
			this.year = year;
		} else
			System.out.println("Invalid");
	}

	public MyDate(String string) {
		String[] temp = string.split(" ");
		this.day = Integer.parseInt(temp[1].substring(0, temp[1].length() - 2));
		this.year = Integer.parseInt(temp[2]);
		switch (temp[0]) {
		case "January":
			this.month = 1;
			break;
		case "February":
			this.month = 2;
			break;
		case "March":
			this.month = 3;
			break;
		case "April":
			this.month = 4;
			break;
		case "May":
			this.month = 5;
			break;
		case "June":
			this.month = 6;
			break;
		case "July":
			this.month = 7;
			break;
		case "August":
			this.month = 8;
			break;
		case "September":
			this.month = 9;
			break;
		case "October":
			this.month = 10;
			break;
		case "November":
			this.month = 11;
			break;
		case "December":
			this.month = 12;
			break;

		}
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public void setMonth(String month) {
		switch (month) {
		case "January":
			this.month = 1;
			break;
		case "February":
			this.month = 2;
			break;
		case "March":
			this.month = 3;
			break;
		case "April":
			this.month = 4;
			break;
		case "May":
			this.month = 5;
			break;
		case "June":
			this.month = 6;
			break;
		case "July":
			this.month = 7;
			break;
		case "August":
			this.month = 8;
			break;
		case "September":
			this.month = 9;
			break;
		case "October":
			this.month = 10;
			break;
		case "November":
			this.month = 11;
			break;
		case "December":
			this.month = 12;
			break;
		}
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	Scanner scanner = new Scanner(System.in);

	public void accept() {
		System.out.println("Enter a string following mm dd yyyy form :");
		String date = scanner.nextLine();
		MyDate tempDate = new MyDate(date);
		System.out.printf("Variable myDate after accept function : %d/%d/%d\n", tempDate.getDay(), tempDate.getMonth(),
				tempDate.getYear());
	}

	public void printString() {
		System.out.println(time);
	}

	public void print() {
		System.out.println(now);
	}

}
