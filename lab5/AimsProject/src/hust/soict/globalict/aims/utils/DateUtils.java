package hust.soict.globalict.aims.utils;

public class DateUtils {
	public static int compareTwoDates(MyDate date1, MyDate date2) {
		if (date1.getYear() < date2.getYear())
			return -1;
		else if (date1.getYear() > date2.getYear())
			return 1;
		else {
			if (date1.getMonth() < date2.getMonth())
				return -1;
			else if (date1.getMonth() > date2.getMonth())
				return 1;
			else {
				if (date1.getDay() < date2.getDay())
					return -1;
				else if (date1.getDay() > date2.getDay())
					return 1;
				else
					return 0;
			}
		}
	}

	public static void sortDates(MyDate[] Datelist) {
		for (int i = 0; i < Datelist.length - 1; i++) {
			for (int j = i + 1; j < Datelist.length; j++) {
				if (compareTwoDates(Datelist[i], Datelist[j]) < 1) {
					MyDate temp = new MyDate();
					temp = Datelist[i];
					Datelist[i] = Datelist[j];
					Datelist[j] = temp;
				}
			}
		}
	}
}
