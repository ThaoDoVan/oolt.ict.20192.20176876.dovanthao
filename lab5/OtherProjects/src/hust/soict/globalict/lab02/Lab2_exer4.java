package hust.soict.globalict.lab02;
import java.util.Scanner;
public class Lab2_exer4 {
	public static void main(String args[]) {
        int h;
        String res ="";
        String record="";
        String space="";
        String character="";
	    Scanner scanner = new Scanner(System.in);
	    System.out.println("Enter the height:");
	    h=scanner.nextInt();
	    int line=1;
	    while(line<=h) {
	    	space ="";
	    	character="";
	    	for(int s =1;s<=h-line;s++)space+=" ";
	    	for(int c=1;c<=2*line-1;c++)character+="*";
	    	res+=space+character+"\n";
	    	res+=record;
	    	line++;
	    }
	    System.out.println(res);
	    
	}
}
