package lab02;
import java.util.Scanner;
public class Lab2_exer6 {
	public static void main(String args[]) {
   int a[]=new int[10];
   int sum=0,avg=0;
   Scanner scanner = new Scanner(System.in);
 
   for(int i=0;i<10;i++) {
	System.out.println("Enter the "+(i+1)+" number");
   a[i]= scanner.nextInt();
   }
   sortASC(a);
   System.out.println("The sorted array is: ");
   show(a);

   for(int i=0;i<10;i++) {
	   sum+=a[i];
   }
   
   System.out.println("The sum of array is "+sum+
		   "\n"+"The avg value of array is "+sum/10 );
      }
   
public static void sortASC(int [] arr) {
    int temp = arr[0];
    for (int i = 0 ; i < arr.length - 1; i++) {
        for (int j = i + 1; j < arr.length; j++) {
            if (arr[i] > arr[j]) {
                temp = arr[j];
                arr[j] = arr[i];
                arr[i] = temp;
            }
        }
    }
}
public static void show(int [] arr) {
    for (int i = 0; i < arr.length; i++) {
        System.out.print(arr[i] + " ");
    }
}

}
